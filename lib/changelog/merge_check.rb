# frozen_string_literal: true

module MergeCheck
  # With async mergeability checks, we need to wait for a newly created MR to be actually
  # mergeable, otherwise the accept_merge fails
  def self.wait_for_mergeability(merge_request)
    retry_count = 0
    while %w[checking unchecked].include?(merge_request.merge_status) && retry_count < 10
      sleep(1)
      merge_request = Gitlab.merge_request(merge_request.project_id, merge_request.iid)
      retry_count += 1
    end
  end
end
